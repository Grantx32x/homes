#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "homes.h"

void readhomes(char *filename, int uzip, int uprice)
{    
    FILE *hf = fopen(filename, "r");
    
    if(!hf)
    {
        printf("Couldn't open listings.csv\n");
    }
    int i = 0;
    int j = 0;
    int k = 0;
    int z, p, a;
    char *ad;
    
    while(fscanf(hf, "%d, %[^,],%d,%d ", &z, ad, &p, &a) != EOF)
    {
        home *h = (home *)malloc(sizeof(home));
        h->zip = z;
        h->addr = ad;
        h->price = p;
        h->area = a;
        //printf("%d, %s, %d, %d\n", h->zip,h->addr, h->price, h->area);

        if(uzip == z)
        {
            printf("%s is within zip code %d\n", ad, uzip);
            j++;
        }
        
        if(uprice <= p +https:// 10000 && uprice >= p - 10000)
        {
            printf("%s priced at %d\n", ad, p);
            k++;
        }
        i++;
    }
    
    printf("There are %d listed homes.\n", i);
    printf("There are %d within that zip code.\n", j);
    printf("There are %d within that price range.\n", k);
}

int main(int argc, char *argv[])
{
    int uzip;
    printf("Enter a zipcode: ");
    scanf("%d", &uzip); 

    int uprice;
    printf("Enter a price: ");
    scanf("%d", &uprice); 
    
    readhomes(argv[1], uzip, uprice);
}
